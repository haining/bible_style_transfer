# coding=utf-8

import os
import torch
import wandb
import datasets
import argparse
import numpy as np
import pandas as pd
from itertools import chain
import pytorch_lightning as pl
from torch.utils.data import Dataset, DataLoader
from pytorch_lightning.loggers import WandbLogger
from transformers import T5TokenizerFast, T5ForConditionalGeneration
from pytorch_lightning.callbacks import (ModelCheckpoint,
                                         LearningRateMonitor,
                                         EarlyStopping,
                                         DeviceStatsMonitor,
                                         TQDMProgressBar)

pl.seed_everything(42)
# constants
MODEL_NAME = 'google/t5-efficient-base-nl24'
BIBLE_DATASET_PATH = 'resource/bible_dataset_hf'
PROJECT_NAME = "bible_style_transfer_with_idio_loss"
BIBLES = ['DRA', 'NCV', 'CEV', 'MSG', 'TLB', 'NET', 'ERV', 'ICB', 'HCSB', 'WEB', 'NLT', 'NIRV', 'NABRE', 'NOG', 'RSV',
          'NRSV', 'KJ21', 'LEB', 'NKJV', 'ASV', 'NLV', 'KJV', 'YLT', 'CEB', 'MEV', 'GW', 'JUB', 'ESV', 'NIV',
          'BBE', 'DARBY', 'ISV', 'GNT']  # 33 bibles, 'GNV' is currently removed from the corpus for its quality
WEIGHTS_DIR = 'resource/weights'
# to log generated texts
PATIENCE = 10  # checks before early stopping
BATCH_SIZE = 8
MAX_SOURCE_TEXT_LENGTH = 256
MAX_TARGET_TEXT_LENGTH = 256
NUM_LOG_TEXT_ROWS = 100
MAX_EPOCHS = 5
LOG_TEXT = True
LOG_TEXT_INTERVAL = 20000
NUM_VAL_SAMPLES = 800
# NUM_VAL_SAMPLES = 'all'
# deepspeed = strategy = DeepSpeedStrategy(logging_batch_size_per_gpu=BATCH_SIZE)

# DECODING_STRATEGIES = ['beam==8', 'top_p==.9']
DECODING_STRATEGIES = ['top_p==.95']
# REPETITION_PENALTIES = [2, 4, 8, 16]
# REPETITION_PENALTY = 1.5
METRICS = ['sacrebleu', 'meteor', 'bertscore']
LR_TRAIN = 5e-5
# idio training
# IDIO_TRAINING = False
# IDIO_SELECTIVE_PENALTY = False
IDIO_TRAINING = True
IDIO_SELECTIVE_PENALTY = True
IDIO_ALPHA = 0.2  # TODO: tune alpha
IDIO_SOFTMAX_TEMPERATURE = 2.0  # TODO: tune temperature


# add constants
# RUN_NAME = 'no_idio_loss'
RUN_NAME = 'idio_loss_selectiveTrue_alpha0.2_temp2.0'
CKPTS_DIR = f'ckpts_{RUN_NAME}'

# # env variable and logger
os.environ["WANDB_CACHE_DIR"] = "wandb"
# wandb_logger = WandbLogger(name=RUN_NAME,
#                            project=PROJECT_NAME,
#                            log_model='all',
#                            save_dir='wandb')


class BibleDataset(Dataset):
    def __init__(
            self,
            # df is df_long generated by `pair_up_style` or `get_random_samples_with_target_style`
            # columned with ['source_style', 'source_text', 'target_style', 'target_text']
            df: pd.DataFrame,
            tokenizer: str = MODEL_NAME,
            max_source_length: int = 256,
            max_target_length: int = 256,
            padding: str = 'max_length'):
        self.df = df
        self.tokenizer = T5TokenizerFast.from_pretrained(tokenizer)
        self.max_source_length = max_source_length
        self.max_target_length = max_target_length
        self.padding = padding

    def __len__(self):
        return self.df.shape[0]

    def __getitem__(self, index: int):
        target_text = self.df.iloc[index]['target_text']
        source_text = self.df.iloc[index]['source_text']
        target_style = self.df.iloc[index]['target_style']
        source_style = self.df.iloc[index]['source_style']
        prompt = f'{target_style}: '
        encoding = self.tokenizer(prompt + source_text,
                                  max_length=self.max_source_length,
                                  padding=self.padding,
                                  truncation=True,
                                  return_tensors='pt')
        target_encoding = self.tokenizer(target_text,
                                         max_length=self.max_source_length,
                                         padding=self.padding,
                                         truncation=True)
        # replace all tokenizer.pad_token_id in the labels by -100 when we want to ignore padding in the loss
        labels = torch.tensor(
            [(label if label != self.tokenizer.pad_token_id else -100) for label in target_encoding['input_ids']],
            dtype=torch.int64)

        return dict(
            input_ids=encoding['input_ids'].flatten(),
            attention_mask=encoding['attention_mask'].flatten(),
            labels=labels.flatten(),
            target_text=target_text,  # for easily computing metrics
            source_text=source_text,
            target_style=target_style,
            source_style=source_style
        )


class BibleDataModule(pl.LightningDataModule):
    """
    DataModule for a denoising autoencoder used for misspellings correction.
    It loads huggingface format dataset ('datasets.dataset_dict.DatasetDict') from disk and return train/val/test
    dataloaders.
    """

    def __init__(
            self,
            tokenizer: str = MODEL_NAME,
            bible_dataset_folder: str = BIBLE_DATASET_PATH,
            batch_size: int = BATCH_SIZE,
            max_source_text_length: int = MAX_SOURCE_TEXT_LENGTH,
            max_target_text_length: int = MAX_TARGET_TEXT_LENGTH,
            num_val_samples: int or str = NUM_VAL_SAMPLES,
    ):
        super().__init__()
        self.bible_dataset = datasets.load_from_disk(bible_dataset_folder)
        self.batch_size = batch_size
        self.tokenizer = tokenizer
        self.max_source_text_length = max_source_text_length
        self.max_target_text_length = max_target_text_length
        self.num_val_samples = num_val_samples
        self.train_dataset = None
        self.val_dataset = None
        self.test_dataset = None

    def pair_up_style(self, split):
        df = pd.DataFrame(self.bible_dataset[split]['translation'])
        df_long = pd.DataFrame(columns=['source_style', 'source_text', 'target_style', 'target_text'])
        # pivot every version and make a long one with above four fields
        for bible_version in BIBLES:
            df_ = pd.melt(df,
                          id_vars=[bible_version],
                          var_name='target_style',
                          value_name='target_text',
                          ignore_index=True)
            df_ = df_.rename(columns={bible_version: 'source_text'})
            df_.insert(loc=0, column="source_style", value=[bible_version] * len(df_))
            df_long = pd.concat([df_long, df_])
        df_long = df_long[(df_long.source_text != '') & (df_long.target_text != '')]

        return df_long

    def setup(self, stage=None):
        # multiple prompts
        train_df_long = self.pair_up_style('train')
        test_df_long = self.pair_up_style('test')
        if self.num_val_samples == 'all':
            val_df_long = self.pair_up_style('validation')
        elif isinstance(self.num_val_samples, int):
            val_df_long = self.pair_up_style('validation')
            # too many validation samples, we randomly choose `NUM_VAL_SAMPLES` to use, set 'all' to use all
            val_df_long = val_df_long.sample(self.num_val_samples)
        else:
            raise ValueError('`num_val_samples` should be `all` or an int.')
        self.train_dataset = BibleDataset(
            train_df_long,
            self.tokenizer,
            self.max_source_text_length,
            self.max_target_text_length)
        self.val_dataset = BibleDataset(
            val_df_long,
            self.tokenizer,
            self.max_source_text_length,
            self.max_target_text_length)
        self.test_dataset = BibleDataset(
            test_df_long,
            self.tokenizer,
            self.max_source_text_length,
            self.max_target_text_length)

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            shuffle=True)

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.batch_size,
            shuffle=False)

    def test_dataloader(self):
        return DataLoader(
            self.test_dataset,
            batch_size=self.batch_size,
            shuffle=False)


class BibleStyleTransferModel(pl.LightningModule):
    """
    An efficient-t5-based translation model built for scientific abstract simplification.
    """

    def __init__(self,
                 model_name: str = MODEL_NAME,
                 tokenizer: str = MODEL_NAME,
                 log_text: bool = LOG_TEXT,
                 decoding_strategies: list = DECODING_STRATEGIES,
                 metrics: list = METRICS,
                 fp16: bool = False,
                 weights_dir: str = WEIGHTS_DIR,
                 idio_training: bool = IDIO_TRAINING,
                 idio_selective_penalty: bool = IDIO_SELECTIVE_PENALTY,
                 idio_softmax_temperature: float = IDIO_SOFTMAX_TEMPERATURE,
                 idio_alpha: float = IDIO_ALPHA,
                 idio_exclude_tokens: list = [],
                 idio_top_diff_tokens: int = -1
                 ):
        super().__init__()
        self.model = T5ForConditionalGeneration.from_pretrained(model_name)
        # self.model.resize_token_embeddings(len(tokenizer))  # fix mismatch embeddings https://github.com/huggingface/transformers/issues/4875
        self.save_hyperparameters()
        self.tokenizer = T5TokenizerFast.from_pretrained(tokenizer)
        self.log_text = log_text
        self.decoding_strategies = decoding_strategies
        self.fp16 = fp16
        # metrics
        self.metrics = metrics
        self.metric_sacrebleu = datasets.load_metric('sacrebleu')
        self.metric_meteor = datasets.load_metric('meteor')
        self.metric_bertscore = datasets.load_metric('bertscore')
        # idio training
        self.idio_training = idio_training
        self.weights_dir = weights_dir
        self.idio_selective_penalty = idio_selective_penalty
        self.idio_softmax_temperature = idio_softmax_temperature
        self.idio_alpha = idio_alpha
        self.idio_exclude_tokens = idio_exclude_tokens
        self.idio_top_diff_tokens = idio_top_diff_tokens
        self.weights_masks = {bible: None for bible in BIBLES}

        if self.idio_training:
            assert weights_dir, RuntimeError('`idio_training` and `weights_dir` not aligned.')
            for bible in BIBLES:
                self.weights_masks[bible] = self.create_normalized_weight_mask(bible)

            # for logging idio training
            self.num_outputs = 0
            self.num_idio = 0

    def forward(self, input_ids, attention_mask, labels):
        output = self.model(input_ids=input_ids, attention_mask=attention_mask, labels=labels)
        return output.loss, output.logits

    def training_step(self, batch, batch_size):
        input_ids = batch['input_ids']
        attention_mask = batch['attention_mask']
        labels = batch['labels']
        decoder_input_ids = self.model._shift_right(labels)
        target_styles = batch['target_style']  # more than one target_style when batch_size > 1

        loss, logits = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels,
            # use_cache=False
        )
        self.log("train_nll_loss", loss, batch_size=batch_size)
        # print loss
        # print(f'nll_loss: {loss.item()}')
        # idio loss objective
        if self.idio_training:
            idio_loss = self.idio_loss(decoder_input_ids,
                                       logits,
                                       target_styles)
            idio_loss_weighted = idio_loss * self.idio_alpha
            loss -= idio_loss_weighted
            self.log("train_idio_loss", idio_loss_weighted.item(), batch_size=batch_size)

        self.log("train_loss", loss, batch_size=batch_size)
        # print loss
        # print(f'idio_loss: {idio_loss_weighted.item()}')
        # print(f'train_loss: {loss.item()}')

        return loss

    def on_validation_start(self):
        # init wandb containers
        if self.log_text and self.global_step % LOG_TEXT_INTERVAL == 0:
            self.log_text_container = []
            for decoding_strategy in self.decoding_strategies:
                self.log_text_container.append(
                    pd.DataFrame(columns=['source_style',
                                          'source_text',
                                          'target_style',
                                          'target_text',
                                          'generated_text'])
                )

    def validation_step(self, batch, batch_size):
        input_ids = batch['input_ids']
        attention_mask = batch['attention_mask']
        labels = batch['labels']

        loss, logits = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels
        )
        # the output of self() is: odict_keys(['loss', 'logits', 'past_key_values', 'encoder_last_hidden_state'])
        self.log("val_loss", loss, batch_size=batch_size)

        # compute metrics only when
        # if self.log_text and self.global_rank == 0 and self.global_step % LOG_TEXT_INTERVAL == 0:
        if self.log_text and self.global_step % LOG_TEXT_INTERVAL == 0:
            self._generate_step(batch, 'val')

        return loss

    def _generate_step(self, batch, data_split: str):
        assert data_split in ['val', 'test']
        # generate given decoding strategy and compute metrics
        source_styles = batch['source_style']
        target_styles = batch['target_style']
        source_texts = batch['source_text']
        target_texts = [[r.strip()] for r in batch['target_text']]  # a list of lists

        # log decoding
        for i, decoding_strategy in enumerate(self.decoding_strategies):
            decoding = decoding_strategy.split('==')[0]
            # assert decoding in ['greedy', 'beam', 'top_p']:
            if decoding == 'greedy':
                output_sequences = self.model.generate(
                    input_ids=batch["input_ids"],
                    attention_mask=batch["attention_mask"],
                    max_length=MAX_TARGET_TEXT_LENGTH,
                    # repetition_penalty=REPETITION_PENALTY,
                    do_sample=False)
            elif decoding == 'beam':
                output_sequences = self.model.generate(
                    input_ids=batch["input_ids"],
                    attention_mask=batch["attention_mask"],
                    max_length=MAX_TARGET_TEXT_LENGTH,
                    num_beams=int(decoding_strategy.split('==')[1]),
                    # repetition_penalty=REPETITION_PENALTY,
                    do_sample=False)
            elif decoding == 'top_p':
                output_sequences = self.model.generate(
                    input_ids=batch["input_ids"],
                    attention_mask=batch["attention_mask"],
                    max_length=MAX_TARGET_TEXT_LENGTH,
                    top_p=float(decoding_strategy.split('==')[1]),
                    # repetition_penalty=REPETITION_PENALTY,
                    do_sample=True)
            # compute common metrics
            predictions = [p.strip() for p in self.tokenizer.batch_decode(output_sequences, skip_special_tokens=True)]
            metrics = self.calculate_common_metrics(source_texts, predictions, target_texts, METRICS)
            # logging
            wandb_logger.log_metrics({f"{data_split}_{decoding_strategy}/sacrebleu": metrics['sacrebleu']})
            wandb_logger.log_metrics({f"{data_split}_{decoding_strategy}/meteor": metrics['meteor']})
            wandb_logger.log_metrics({f"{data_split}_{decoding_strategy}/bertscore": metrics['bertscore_f1']})

            _df = pd.DataFrame({
                'source_style': source_styles,
                'source_text': source_texts,
                'target_style': target_styles,
                'target_text': list(chain(*target_texts)),
                'generated_text': predictions,
            })
            self.log_text_container[i] = pd.concat([self.log_text_container[i], _df], ignore_index=True)

    def on_validation_end(self):
        # log texts
        data_split = 'val'
        if self.log_text and self.global_step % LOG_TEXT_INTERVAL == 0:
            for i, decoding_strategy in enumerate(self.decoding_strategies):
                wandb_logger.log_text(
                    key=f"{data_split}_{decoding_strategy}/global_step_{self.global_step}",
                    dataframe=self.log_text_container[i].loc[:NUM_LOG_TEXT_ROWS - 1, :])

    def test_step(self, batch, batch_size):
        input_ids = batch['text_input_ids']
        attention_mask = batch['text_attention_mask']
        labels = batch['labels']

        loss, outputs = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels
        )
        # the output of self() is: odict_keys(['loss', 'logits', 'past_key_values', 'encoder_last_hidden_state'])
        self.log("test_loss", loss, batch_size=batch_size)
        if self.log_text:
            self._generate_step(batch, 'test')

        return loss

    def create_normalized_weight_mask(self, bible: str):
        """
        idio_top_diff_tokens=-1 means use all tokens positively correlated to authorial style.
        """
        weights = []
        df = pd.read_csv(os.path.join(self.weights_dir, f'{bible}.csv'))
        for row in df.itertuples():
            if row.token not in self.idio_exclude_tokens:
                weights.append((row.id, row.weight))
        weights = [w for w in weights if
                   w[1] > 0]  # only need positive tokens that contribute to authorial idiosyncrasies
        if self.idio_top_diff_tokens > -1:
            weights = weights[-self.idio_top_diff_tokens:]

        ids = [x[0] for x in weights]
        weights = torch.tensor([x[1] for x in weights])
        normalized_weights = torch.nn.functional.softmax(weights / self.idio_softmax_temperature, dim=-1)
        weights_mask = torch.zeros(self.model.config.vocab_size).float()
        for i in range(len(ids)):
            weights_mask[ids[i]] = normalized_weights[i]
        return weights_mask

    def idio_loss(self, decoder_input_ids, logits, target_styles):
        """
        Calculates token level idio loss, enlightened by unlikelihood training. Implementation refers to:
        https://github.com/AshOlogn/Paragraph-level-Simplification-of-Medical-Texts/blob/main/modeling/finetune.py
        https://github.com/facebookresearch/ParlAI/blob/main/projects/dialogue_unlikelihood/agents.py
        decoder_input_ids - (n, s)
        logits            - (n, s, v)
        weight_mask       - (v,)
        target_styles     - (n,)
        """
        # log negative probability
        probs = torch.nn.functional.softmax(logits, dim=-1)
        clamp_min = 1e-6 if self.fp16 else 1e-20
        neg_probs = torch.clamp(1.0 - probs, min=clamp_min)
        # neg_probs = 1-probs
        # neg_probs += (neg_probs == 0).float() * 1e-8
        log_neg_probs = torch.log(neg_probs)  # (n, s, v)

        # attention mask
        attention_mask = decoder_input_ids.eq(1).eq(0).float()  # (n, s)
        attention_mask = attention_mask.unsqueeze(2).expand(-1, -1, logits.shape[2])  # (n, s, v)
        log_neg_probs_masked = log_neg_probs * attention_mask  # (n, s, v)

        # apply weight vector to the log probability tensor
        n, s = logits.size()[:2]  # logits.size(): torch.Size([8, 256, 32128])
        # print(f"logits.size(): {logits.size()}")
        weights_masks = torch.empty(n, s, self.model.config.vocab_size).to('cuda:1')
        for i, target_style in enumerate(target_styles):  # (interest_)bible, target_style, prompt are the same thing
            weights_masks[i] = self.weights_masks[target_style].unsqueeze(0).expand(s, -1)
            # print(f"weights_masks.size(): {weights_masks.size()}")
        # weight_mask_expanded = weight_mask.unsqueeze(0).unsqueeze(0).expand(n, s, -1)
        weighted_probs = log_neg_probs_masked * weights_masks  # (n, s, v)

        if self.idio_selective_penalty:
            indices = torch.argmax(logits, dim=-1)  # (n, s)
            indices_mask = torch.nn.functional.one_hot(indices, num_classes=logits.shape[-1])  # (n, s, v)
            weighted_probs *= indices_mask

            # log the number of tokens that idio loss affects
            count_vec = (weighted_probs != 0).int()  # (n, s, v)
            count_vec = torch.sum(count_vec, dim=-1)  # (n, s)
            pad_mask = decoder_input_ids.eq(1).eq(0).int()
            count_vec *= pad_mask
        self.num_outputs += pad_mask.sum()
        self.num_idio += count_vec.sum()
        self.log("num_outputs", self.num_outputs, batch_size=BATCH_SIZE)
        self.log("num_outputs_impacted_by_idio_loss", self.num_idio, batch_size=BATCH_SIZE)

        return -torch.sum(weighted_probs)

    def configure_optimizers(self):
        return torch.optim.AdamW(self.parameters(), lr=LR_TRAIN)

    def calculate_common_metrics(self,
                                 source_texts: list,
                                 predictions: list,
                                 target_texts: list,
                                 metrics: list = ['sacrebleu', 'bertscore', 'meteor']):
        """
        Computes common metrics.
        Args:
            predictions: list of str, generated texts
            target_texts: list of list, by convention found in the datasets library
            metrics: list of str, should be in ['sacrebleu', 'bertscore', 'meteor']

        Returns:
            dict, keyed by `metrics`
        """
        metrics_dict = {}
        predictions = [p.strip() for p in predictions]
        target_texts = [[_r.strip() for _r in r] for r in target_texts]
        # source_texts = [r.strip() for r in source_texts]  # only sari uses `source_texts`
        for metric in metrics:
            assert metric in ['sacrebleu', 'bertscore', 'meteor']
            if metric == 'sacrebleu':
                metrics_dict.update({'sacrebleu':
                    np.mean(
                        self.metric_sacrebleu.compute(predictions=predictions, references=target_texts)[
                            'score'])})
            elif metric == 'meteor':
                metrics_dict.update({'meteor': np.mean(
                    self.metric_meteor.compute(predictions=predictions, references=target_texts)['meteor'])})
            elif metric == 'bertscore':
                bertscore = self.metric_bertscore.compute(predictions=predictions, references=target_texts, lang='en')
                metrics_dict.update({'bertscore_f1': np.mean(bertscore['f1'])})
                metrics_dict.update({'bertscore_precision': np.mean(bertscore['precision'])})
                metrics_dict.update({'bertscore_recall': np.mean(bertscore['recall'])})

        return metrics_dict


if __name__ == '__main__':
    # init logger
    wandb_logger = WandbLogger(name=RUN_NAME,
                               project=PROJECT_NAME,
                               log_model='all',
                               save_dir='wandb')
    # prepare the data
    dm = BibleDataModule(tokenizer=MODEL_NAME)

    # prepare the model
    model = BibleStyleTransferModel(model_name=MODEL_NAME,
                                    tokenizer=MODEL_NAME)

    # prepare the trainer
    trainer = pl.Trainer(
        # fast dev
        # fast_dev_run=500,
        # gpu
        gpus=[1],
        # gpus=[0, 1, 2, 3],
        accelerator="gpu",
        # strategy=deepspeed,
        # mixed precision training
        # precision=16,
        # step
        max_epochs=MAX_EPOCHS,
        log_every_n_steps=100,
        val_check_interval=2000,
        callbacks=[ModelCheckpoint(
            dirpath=CKPTS_DIR,
            every_n_train_steps=1000,
            filename='{epoch}-{step}-{val_loss:.3f}',
            save_top_k=3,
            verbose=True,
            monitor='val_loss',
            mode='min'),
            LearningRateMonitor("step"),
            EarlyStopping(monitor="val_loss", min_delta=0, patience=PATIENCE, verbose=False,
                          mode="min"),
            DeviceStatsMonitor(),
            TQDMProgressBar(refresh_rate=1)],
        # misc
        deterministic=True,
        enable_progress_bar=True,
        move_metrics_to_cpu=True,
        logger=wandb_logger,
        default_root_dir=CKPTS_DIR,
    )

    trainer.fit(model=model, datamodule=dm)
