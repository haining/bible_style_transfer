import os
import datasets
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from transformers import T5TokenizerFast
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import Normalizer, StandardScaler

BIBLE_READABILITY = 'resource/bible_readability.pkl'
BIBLE_DATASET_PATH = 'resource/bible_dataset_hf'
MODEL_NAME = 'google/t5-efficient-base-nl24'
WEIGHTS_DIR = 'resource/weights'
tokenizer = T5TokenizerFast.from_pretrained(MODEL_NAME)
# read in bibles
BIBLES = ['DRA', 'NCV', 'CEV', 'MSG', 'TLB', 'NET', 'ERV', 'ICB', 'HCSB', 'WEB', 'NLT', 'NIRV', 'NABRE', 'NOG', 'RSV',
          'NRSV', 'KJ21', 'LEB', 'NKJV', 'ASV', 'NLV', 'KJV', 'YLT', 'CEB', 'MEV', 'GW', 'JUB', 'ESV', 'NIV',
          'BBE', 'DARBY', 'ISV', 'GNT']  # 33 bibles, 'GNV' is currently removed from the corpus for its quality


def get_vector(raw, tokenizer=tokenizer):
    """
    Tokenizes a raw text into vectors.
    """
    token_ids = tokenizer.encode(raw)[:-1]  # remove ending token `</s>`
    vector = np.zeros(tokenizer.vocab_size, dtype=np.int16)
    for id_ in token_ids:
        vector[id_] += 1
    return vector


def construct_bible_ovr_corpus(interest_bible: str = 'ASV',
                               background_bibles='rest',
                               bible_dataset_folder: str = BIBLE_DATASET_PATH):
    """
    This function organizes an ad hoc balanced in a one-vs-rest manner.
    It first finds out the available spans of `interest_bible`, then randomly choose the same number of spans from other
    versions of bible as specified in `background_bibles`.
    """
    assert interest_bible in BIBLES, print(f'`interest_bible` should one of {BIBLES}.')
    assert background_bibles == 'rest', print('Set `background_bibles` to "rest", for now.')
    rng = np.random.default_rng(42)
    bible_dataset = datasets.load_from_disk(bible_dataset_folder)
    # containers
    X, y = [], []
    for bible_spans in bible_dataset['train']['translation']:
        if bible_spans.get(interest_bible):
            interest_bible_span = bible_spans.get(interest_bible)
            background_bible_ = rng.choice(list([k for k, v in bible_spans.items() if v != '']), 1).item()
            background_bible_span = bible_spans.get(background_bible_)
            X.append(get_vector(interest_bible_span))
            y.append(1)  # bible version of interest specified as 1
            X.append(get_vector(background_bible_span))
            y.append(0)

    return X, y


def get_most_differentiable_features(interest_bible,
                                     learner_name='logistic_regression',
                                     tokenizer=tokenizer,
                                     weights_dir=WEIGHTS_DIR):
    assert learner_name in ['logistic_regression', 'svm'], print(
        f"Set `learner_name` as one of {['logistic_regression', 'svm']}.")
    X, y = construct_bible_ovr_corpus(interest_bible)
    if learner_name == 'logistic_regression':
        learner = ("logistic_regression", LogisticRegression(C=1, solver="lbfgs", max_iter=1e9))
    elif learner_name == 'svm':
        learner = ("linear_svm", SVC(C=1, kernel="linear", max_iter=-1))
    pipeline = Pipeline([("normalizer", Normalizer(norm="l1")), ("scaler", StandardScaler()), learner])
    pipeline.fit(X, y)
    # dump(pipeline, f'resource/{interest_bible}_{learner_name}_pipeline.bak')

    sorted_vocab = [tokenizer.decode([idx], clean_up_tokenization_spaces=False) for idx in range(tokenizer.vocab_size)]
    weights = np.squeeze(pipeline['logistic_regression'].coef_, axis=0).tolist()

    # sorted_weights = filter(lambda x: len(x[1].strip()) > 0, zip(range(tokenizer.vocab_size), sorted_vocab, weights))
    sorted_weights_ = zip(range(tokenizer.vocab_size), sorted_vocab, weights)
    sorted_weights = list(sorted(sorted_weights_, key=lambda x: x[2]))

    df = pd.DataFrame.from_records(sorted_weights, columns=['id', 'token', 'weight'])
    df.to_csv(os.path.join(weights_dir, f'{interest_bible}.csv'), index=False)


if __name__ == "__main__":
    for bible in BIBLES:
        get_most_differentiable_features(interest_bible=bible,
                                         learner_name='logistic_regression',
                                         tokenizer=tokenizer,
                                         weights_dir='resource/weights')
