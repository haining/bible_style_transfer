import pickle
import pandas as pd
from utilities import jsonl2list
from readability import Readability
from transformers import T5TokenizerFast

BIBLE_READABILITY = 'resource/bible_readability.pkl'
BIBLE_DATASET_PATH = 'resource/bible_dataset_hf'
MODEL_NAME = 'google/t5-efficient-base-nl24'
tokenizer = T5TokenizerFast.from_pretrained(MODEL_NAME)
# read in bibles
BIBLES = ['DRA', 'NCV', 'CEV', 'MSG', 'TLB', 'NET', 'ERV', 'ICB', 'HCSB', 'WEB', 'NLT', 'NIRV', 'NABRE', 'NOG', 'RSV',
          'NRSV', 'KJ21', 'LEB', 'NKJV', 'ASV', 'NLV', 'KJV', 'YLT', 'CEB', 'MEV', 'GW', 'JUB', 'ESV', 'NIV',
          'BBE', 'DARBY', 'ISV', 'GNT']  # 33 bibles, 'GNV' is currently removed from the corpus for its quality


train_file_path = 'resource/bible_corpus/bible_train.jsonl'
val_file_path = 'resource/bible_corpus/bible_validation.jsonl'
test_file_path = 'resource/bible_corpus/bible_test.jsonl'

bible_corpus = []
for p in [train_file_path, val_file_path, test_file_path]:
    split_with_key = jsonl2list(p)
    for dict_ in split_with_key:
        bible_corpus.append(dict_['translation'])

bible_merged_by_version = {bible: {} for bible in BIBLES}
for bible in BIBLES:
    for dict_ in bible_corpus:
        if dict_.get(bible):
            bible_merged_by_version[bible] += dict_.get(bible)
            bible_merged_by_version[bible] += ' '
# calculate readability
bible_readability = {bible: {} for bible in BIBLES}
for bible in BIBLES:
    r = Readability(bible_merged_by_version.get(bible))
    # Flesch-Kincaid Reading Ease and Automated Readability Index (ARI)
    bible_readability[bible]['Flesch'] = r.flesch()
    bible_readability[bible]['ARI'] = r.ari()

# save readability scores
pickle.dump(bible_readability, open(BIBLE_READABILITY, 'wb'))
bible_flesch = {bible: None for bible in BIBLES}
bible_ari = {bible: None for bible in BIBLES}
for bible in BIBLES:
    bible_flesch[bible] = bible_readability[bible]['Flesch'].score
    bible_ari[bible] = bible_readability[bible]['ARI'].score
pd.DataFrame.from_dict(bible_flesch, orient='index').to_csv('resource/bible_flesch.csv', index=False)
pd.DataFrame.from_dict(bible_ari, orient='index').to_csv('resource/bible_ari.csv', index=False)
