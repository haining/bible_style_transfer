#!/usr/bin/env python
# coding=utf-8

"""
This module
1. reads in `bible-dataset-2020/combinedBibles` corpus (with GNV dropped).
2. splits data into train/dev/test as found in Carlson et al. (2018)
3. saves files into .jsonl format as per to convention in Datasets library.

"""

__author__ = "hw56@indiana.edu"
__version__ = "1.0.0"
__license__ = "ISC"

import os
import re
import json
import chardet
import datasets
from utilities import list2jsonl

SEED = 42
BIBLE_DIR = "/srv/datasets/bible-dataset-2020/combinedBibles"
BIBLE_DATASET_PATH = 'resource/bible_dataset_hf'

# line indices of each book used for dev and test
VAL_TEST_SPANS = {'validation': [[1485, 2300],      # 1 Kings, the sequence of chapters reads: 1, 10, 11 ,..., 2, 20,...3, ...9
                                 [31047, 31099],    # Zephaniah
                                 [22635, 23312],    # Mark
                                 [7283, 7377]],     # Colossians
                  'test': [[19798, 20415],          # Judges
                           [2406, 3215],            # 1 Samuel
                           [26276, 26379],          # Philippians
                           [13780, 14082]]          # Hebrews
                  }

train_file_path = 'resource/bible_corpus/bible_train.jsonl'
val_file_path = 'resource/bible_corpus/bible_validation.jsonl'
test_file_path = 'resource/bible_corpus/bible_test.jsonl'

if __name__ == '__main__':

    # find start and end line indices for to align spans cross bibles, drop 'GNV'
    bibles_dir_entry = [b for b in os.scandir(BIBLE_DIR) if (b.name.endswith("txt") and not b.name.startswith('GNV'))]
    aligned_spans_train = []
    aligned_spans_val = []
    aligned_spans_test = []
    line_idx = 0
    while line_idx < 31099:  # there are 31099 lines in each .txt
        aligned_span_dict = {}
        span_end_indices = []
        # find the next line_idx
        for bible in bibles_dir_entry:
            title = bible.name.split('.')[0]
            # if title == "JUB":  # only JUB.txt is encoded with utf-8, otherwise encoded with ISO-8859-1
            #     bible_line = open(bible.path, 'r', encoding='ISO-8859-1').read().splitlines()[line_idx]
            if title == "NLV":  # NLV is encoded with Windows-1252, otherwise encoded with utf8
                bible_line = open(bible.path, 'r', encoding='Windows-1252').read().splitlines()[line_idx]
            else:
                bible_line = open(bible.path, 'r', encoding='utf-8').read().splitlines()[line_idx]
            # check if it stands alone across different versions
            # bible_line never be ""
            span = re.match('\s*(\d{1,4})-(\d{1,4})\s', bible_line)
            if span:
                span_end_indices.append(line_idx + int(span.group(2)) - int(span.group(1)) + 1)
            else:
                span_end_indices.append(line_idx + 1)
        span_end_idx = max(span_end_indices)

        # read in lines in the span
        for bible in bibles_dir_entry:
            title = bible.name.split('.')[0]
            if span_end_idx - line_idx > 1:  # multiple lines
                if title == "NLV":
                    span_line_list = open(bible.path, 'r', encoding='Windows-1252').read().splitlines()[
                                     line_idx: span_end_idx]
                else:
                    span_line_list = open(bible.path, 'r', encoding='utf-8').read().splitlines()[
                                     line_idx: span_end_idx]
            else:  # one line
                if title == "NLV":
                    span_line_list = [open(bible.path, 'r', encoding='Windows-1252').read().splitlines()[line_idx]]
                else:
                    span_line_list = [open(bible.path, 'r', encoding='utf-8').read().splitlines()[line_idx]]
            aligned_span_dict.update({title: span_line_list})

        for data_split, idx_span in VAL_TEST_SPANS.items():
            for start_idx, end_idx in idx_span:
                # print(start_idx, end_idx)
                # idx_span starts from 1, while python (line_idx and span_end_idx) starts from 0
                if line_idx >= start_idx - 1 and span_end_idx < end_idx + 1:
                    if data_split == 'validation':
                        aligned_spans_val.append({'translation': aligned_span_dict})
                        aligned_span_dict = None
                    elif data_split == 'test':
                        aligned_spans_test.append({'translation': aligned_span_dict})
                        aligned_span_dict = None
        if aligned_span_dict:
            aligned_spans_train.append({'translation': aligned_span_dict})

        # aligned_spans.append({'translation': aligned_span_dict})
        # locate next line_idx
        line_idx = span_end_idx
        print(line_idx)
        # line_idx = span_end_idx if (span_end_idx - line_idx > 1) else span_end_idx+1

    # clean up aligned_spans because each string has a leading artificial indicator
    clean_aligned_spans_train = []
    for aligned_span in aligned_spans_train:
        clean_aligned_span_dict = {}
        for title, contents in aligned_span['translation'].items():  # contents is a list of strings
            clean_contents = ''
            for content in contents:
                content = re.sub("[\(\[]|(\s*(\d{1,3})-(\d{1,3})|\s*(\d{1,3}))", "", content).strip()
                clean_contents = clean_contents + " " + content
            clean_aligned_span_dict.update({title: clean_contents.strip().replace(']', "").replace('[', "").replace("¶ ", '').replace("¶", '')})
        clean_aligned_spans_train.append({'translation': clean_aligned_span_dict})
    # clean up aligned_spans because each string has a leading artificial indicator
    clean_aligned_spans_val = []
    for aligned_span in aligned_spans_val:
        clean_aligned_span_dict = {}
        for title, contents in aligned_span['translation'].items():  # contents is a list of strings
            clean_contents = ''
            for content in contents:
                content = re.sub("[\(\[]|(\s*(\d{1,3})-(\d{1,3})|\s*(\d{1,3}))", "", content).strip()
                clean_contents = clean_contents + " " + content
            clean_aligned_span_dict.update({title: clean_contents.strip().replace(']', "").replace('[', "")})
        clean_aligned_spans_val.append({'translation': clean_aligned_span_dict})
    # clean up aligned_spans because each string has a leading artificial indicator
    clean_aligned_spans_test = []
    for aligned_span in aligned_spans_test:
        clean_aligned_span_dict = {}
        for title, contents in aligned_span['translation'].items():  # contents is a list of strings
            clean_contents = ''
            for content in contents:
                content = re.sub("[\(\[]|(\s*(\d{1,3})-(\d{1,3})|\s*(\d{1,3}))", "", content).strip()
                clean_contents = clean_contents + " " + content
            clean_aligned_span_dict.update({title: clean_contents.strip().replace(']', "").replace('[', "").replace("¶ ", "")})
        clean_aligned_spans_test.append({'translation': clean_aligned_span_dict})

    # # split data_dict list into train/dev/test
    # train_data, val_test_data = train_test_split(clean_aligned_spans, test_size=200, random_state=SEED)
    # val_data, test_data = train_test_split(val_test_data, test_size=100, random_state=SEED)

    # save clean aligned splits to jsonl format
    list2jsonl(clean_aligned_spans_train, train_file_path)
    list2jsonl(clean_aligned_spans_val, val_file_path)
    list2jsonl(clean_aligned_spans_test, test_file_path)
    # convert bible jsonl into datasets.dataset_dict.DatasetDict
    bible = datasets.load_dataset('json', data_files={'train': train_file_path,
                                                      'validation': val_file_path,
                                                      'test': test_file_path})
    bible.save_to_disk(BIBLE_DATASET_PATH)

# for test below


# load jsonl
# clean_aligned_spans_val = []
# for line in open(val_file_path, 'r').readlines():
#     clean_aligned_spans_val.append(json.loads(line))
# clean_aligned_spans_train = []
# for line in open(train_file_path, 'r').readlines():
#     clean_aligned_spans_train.append(json.loads(line))
# clean_aligned_spans_test = []
# for line in open(test_file_path, 'r').readlines():
#     clean_aligned_spans_test.append(json.loads(line))


# check encoding
# bibles_dir_entry = [b for b in os.scandir(BIBLE_DIR) if b.name.endswith("txt")]
# for bible in bibles_dir_entry:
#     print(bible.name, chardet.detect(open(bible, 'rb').read()))


# aligned_spans
# [aligned_spans[0]]

# head_T = []
# for bible in bibles_dir_entry:
#     bible_lines = open(bible.path, 'r', encoding='ISO-8859-1').read().splitlines()
#     for line in bible_lines:
#         if line.startswith("\x9d"):
#             head_T.append(line)
#             # heading_char.append(line[0])
# # heading_counter = Counter(heading_char)
# # heading_counter.most_common(30)
