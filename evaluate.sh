#!/bin/bash

python3 evaluate.py -c rj -m svm -t braiv_baseline_NIRV
python3 evaluate.py -c rj -m svm -t braiv_baseline_NABRE
python3 evaluate.py -c rj -m svm -t braiv_baseline_KJV
python3 evaluate.py -c rj -m svm -t braiv_baseline_YLT
python3 evaluate.py -c rj -m svm -t braiv_idio_NIRV
python3 evaluate.py -c rj -m svm -t braiv_idio_NABRE
python3 evaluate.py -c rj -m svm -t braiv_idio_KJV
python3 evaluate.py -c rj -m svm -t braiv_idio_YLT