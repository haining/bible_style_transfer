
import pickle
import datasets
import numpy as np
import utilities as ut


def calculate_common_metrics(predictions: list,
                             target_texts: list,
                             metrics: list = ['sacrebleu', 'bertscore', 'meteor']):
    """
    Computes common metrics.
    Args:
        predictions: list of str, generated texts
        target_texts: list of list, by convention found in the datasets library
        metrics: list of str, should be in ['sacrebleu', 'bertscore', 'meteor']

    Returns:
        dict, keyed by `metrics`
    """
    metrics_dict = {}
    predictions = [p.strip() for p in predictions]
    target_texts = [[r.strip()] for r in target_texts]
    for metric in metrics:
        assert metric in ['sacrebleu', 'bertscore', 'meteor']
        if metric == 'sacrebleu':
            metrics_dict.update({'sacrebleu':
                np.mean(
                    metric_sacrebleu.compute(predictions=predictions, references=target_texts)[
                        'score'])})
        elif metric == 'meteor':
            metrics_dict.update({'meteor': np.mean(
                metric_meteor.compute(predictions=predictions, references=target_texts)['meteor'])})
        elif metric == 'bertscore':
            bertscore = metric_bertscore.compute(predictions=predictions, references=target_texts, lang='en', device=1)
            metrics_dict.update({'bertscore_f1': np.mean(bertscore['f1'])})
            metrics_dict.update({'bertscore_precision': np.mean(bertscore['precision'])})
            metrics_dict.update({'bertscore_recall': np.mean(bertscore['recall'])})

    return metrics_dict


# # locate BBE to KJV in the test set
# bbe = []
# kjv = []
# test_file_path = 'resource/bible_corpus/bible_test.jsonl'
# bible_test = datasets.load_dataset('json', data_files={'test': test_file_path})
# for verse in bible_test['test']:
#     if verse['translation']['BBE'] and verse['translation']['KJV']:
#         bbe.append(verse['translation']['BBE'])
#         kjv.append(verse['translation']['KJV'])
# bbe2kjv_greedy = transfer_style(frozen_pl_model=model,
#                                 task_prefix:"KJV: ",
#                                 source_texts=bbe,
#                                 target_texts=kjv,
#                                 top_p=.95,
#                                 do_sample=True)

if __name__ == "__main__":
    # load metrics
    metric_sacrebleu = datasets.load_metric('sacrebleu')
    metric_meteor = datasets.load_metric('meteor')
    metric_bertscore = datasets.load_metric('bertscore')

    text_label = pickle.load(open('resource/adversarial_samples/rj_control/baseline_4bible.pkl', 'rb'))

    metrics = []
    for bible in ['NIRV', 'NABRE', 'KJV', 'YLT']:
        # print(f'{len(text_label[bible]["texts"])}')
        # print(f'{len(text_label[bible]["original_texts"])}')
        metrics.append({bible: calculate_common_metrics(predictions=text_label[bible]['texts'],
                                                 target_texts=text_label[bible]['original_texts'])})

    pickle.dump(metrics, open('results/metrics_rj_baseline_4bible.pkl', 'wb'))
    # a = pickle.load(open('results/metrics_rj_baseline_4bible.pkl', 'rb'))
