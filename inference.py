# coding=utf-8

import os
import wandb
import datasets
# import evaluate
import numpy as np
import pickle
import pandas as pd
from time import time
import utilities as ut
from functools import reduce
from nltk.tokenize import sent_tokenize
from transformers import T5TokenizerFast
from train import BibleStyleTransferModel, MAX_SOURCE_TEXT_LENGTH, MAX_TARGET_TEXT_LENGTH

DEFENSE_BIBLES = ['NIRV', 'NABRE', 'KJV', 'YLT']


def _transfer_style(frozen_pl_model,
                    task_prefix: str,
                    source_texts: list,
                    top_p: float = .95,
                    do_sample: bool = True,
                    max_source_length: str = MAX_SOURCE_TEXT_LENGTH):
    """
    Denoises a list of noisy English strings which is smaller than MAX_SOURCE_TEXT_LENGTH.
    If a list of reference strings are given, outputs sacrebleu score.
    If `misspellings` and `correct_spellings` are given, outputs correction accuracy as well.

    # https://huggingface.co/docs/transformers/main/en/main_classes/text_generation#transformers.generation_utils.GenerationMixin.generate
    greedy decoding by calling greedy_search() if num_beams=1 and do_sample=False.
    multinomial sampling by calling sample() if num_beams=1 and do_sample=True.
    beam-search decoding by calling beam_search() if num_beams>1 and do_sample=False.
    """

    encoding = tokenizer(
        [task_prefix + t for t in source_texts],
        max_length=max_source_length,
        padding='max_length',
        truncation=True,
        return_tensors='pt').to(device)

    decoded_ids = frozen_pl_model.model.generate(
        input_ids=encoding['input_ids'],
        attention_mask=encoding['attention_mask'],
        max_new_tokens=MAX_TARGET_TEXT_LENGTH,
        top_p=top_p,
        do_sample=do_sample
    ).to(device)
    predictions = [p.strip() for p in tokenizer.batch_decode(decoded_ids, skip_special_tokens=True)]

    return predictions


def transfer_style(frozen_pl_model,
                   task_prefix: str,
                   source_texts: list,
                   top_p: float = .95,
                   do_sample: bool = True,
                   max_source_length: str = MAX_SOURCE_TEXT_LENGTH):
    """
    Chunks down texts into pieces smaller than MAX_SOURCE_TEXT_LENGTH, transfers style into pieces, and assembles back.
    """
    start = time()
    generated_texts = []
    for source_text in source_texts:
        # source_text = ebg_test_text[0]
        sents = sent_tokenize(source_text)
        chunks = []
        chunk_ = ''
        chunk_len = 0
        total_sents = len(sents)  #
        for i, sent in enumerate(sents):
            sent_len = len(tokenizer(sents[i])['input_ids'])
            if sent_len > MAX_SOURCE_TEXT_LENGTH:
                print(f"There is a sentence has {sent_len} tokens, longer than {MAX_SOURCE_TEXT_LENGTH}.")
            if sent_len + chunk_len < MAX_SOURCE_TEXT_LENGTH:
                chunk_ += ' ' + sent  # add a whitespace here
                chunk_len += 1 + sent_len
                if i == total_sents - 1:
                    chunks.append(chunk_)
            else:
                chunks.append(chunk_)
                chunk_ = sents[i]
                chunk_len = sent_len

        new_chunks = _transfer_style(frozen_pl_model=frozen_pl_model,
                                     task_prefix=task_prefix,
                                     source_texts=chunks,
                                     top_p=top_p,
                                     do_sample=do_sample,
                                     max_source_length=max_source_length)

        generated_texts.append(reduce(lambda a, b: a + ' ' + b, new_chunks))
    print(f'Finish {task_prefix} in {str(time() - start)[:5]} seconds.')
    return generated_texts


def df2corpus(
        model_prefix: str = 'attacks_braiv_baseline',
        pkl_path: str = 'resource/adversarial_samples/rj_control/baseline_4bible.pkl',
        corpus_dir: str = "resource/defending-against-authorship-attribution-corpus/corpus"):
    """
    Saves adversarial samples in `resource/adversarial_samples` to the correponding corpus (i.e., rj and ebg).

    """
    DEFENSE_BIBLES = ['NIRV', 'NABRE', 'KJV', 'YLT']
    text_label = pickle.load(open(pkl_path, 'rb'))
    for bible in DEFENSE_BIBLES:
        os.mkdir(os.path.join(corpus_dir, f'{model_prefix}_{bible}'))
        for i, label in enumerate(text_label[bible]['labels']):
            with open(os.path.join(corpus_dir, f'{model_prefix}_{bible}', f'{label}.txt'), 'w') as f:
                f.write(text_label[bible]['texts'][i])
    print('Done.')


if __name__ == "__main__":
    device = "cuda:1"
    MODEL_NAME = 'google/t5-efficient-base-nl24'
    tokenizer = T5TokenizerFast.from_pretrained(MODEL_NAME)
    BASELINE_CKPT_PATH = 'ckpts_no_idio_loss/epoch=0-step=306000-val_loss=1.028.ckpt'
    # IDIO_CKPT_PATH = 'ckpts_idio_loss_selectiveTrue_alpha0.2_temp2.0/epoch=0-step=158000-val_loss=1.126.ckpt'
    # load ckpts
    model = BibleStyleTransferModel.load_from_checkpoint(BASELINE_CKPT_PATH).to(device)
    model.freeze()

    # read in RJ-control
    _, _, rj_test_text, rj_test_label = ut.get_data_from_rj('control',
                                                            corpus_dir="resource/defending-against-authorship-attribution-corpus/corpus")
    # transfer the testing samples with 4 styles
    adversarial_rj_sample = {bible: {'texts': None,
                                     'original_texts': rj_test_text,
                                     'labels': rj_test_label} for bible in DEFENSE_BIBLES}
    for bible in DEFENSE_BIBLES:
        adversarial_rj_sample[bible]['texts'] = transfer_style(model,
                                                               task_prefix=bible,
                                                               source_texts=rj_test_text,
                                                               top_p=.90)
    # save to 'resource/adversarial_samples/rj_control'
    pickle.dump(adversarial_rj_sample, open('resource/adversarial_samples/rj_control/baseline_4bible.pkl', 'wb'))

    df2corpus()
    # df2corpus(
    #     model_prefix='attacks_braiv_idio',
    #     pkl_path='resource/adversarial_samples/rj_control/idio_4bible.pkl',
    #     corpus_dir="resource/defending-against-authorship-attribution-corpus/corpus")

    # adversarial_rj_sample_df = pd.DataFrame(adversarial_rj_sample)
    # adversarial_rj_sample_df.to_csv('resource/adversarial_samples/rj_control/idio_4bible.csv')

    # read in EBG, all writer share training, the task doesn't matter here
    # will use the first training sample as the testing
    # _, _, ebg_test_text, ebg_test_label, _, _ = ut.get_data_from_ebg('imitation',
    #                                                                  corpus_dir="resource/Drexel-AMT-Corpus",
    #                                                                  dev=True)
    # adversarial_ebg_sample = {bible: {'texts': None, 'labels': ebg_test_label} for bible in DEFENSE_BIBLES}
    # for bible in DEFENSE_BIBLES:
    #     adversarial_ebg_sample[bible]['texts'] = transfer_style(model,
    #                                                             task_prefix=bible,
    #                                                             source_texts=ebg_test_text,
    #                                                             top_p=.90)
    # # save to 'resource/adversarial_samples/ebg'
    # adversarial_ebg_sample_df = pd.DataFrame(adversarial_ebg_sample)
    # adversarial_ebg_sample_df.to_csv('resource/adversarial_samples/ebg/idio_4bible.csv')

    # a = transfer_style(baseline,
    #                task_prefix=bible,
    #                source_texts=rj_test_text[:2],
    #                top_p=.95)
