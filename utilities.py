import os
import re
import json
import pickle
import chardet
import numpy as np
import pandas as pd
from itertools import combinations
from writeprints_static import WriteprintsStatic


def list2jsonl(data_dict_list, path):
    with open(path, 'w') as f:
        for line in data_dict_list:
            json.dump(line, f)
            f.write('\n')


def jsonl2list(path):
    data_dict_list = []
    with open(path, 'r') as f:
        data_dict_list_raw = list(f)
    for elem in data_dict_list_raw:
        data_dict_list.append(json.loads(elem))
    return data_dict_list


def pair_up_source_target_bible(list_dict_dicts):
    """
    Pairs up source and target bible verses in variables clean_aligned_spans(_train/val/test).
    They are a list of dict of dicts.
    """
    new_list_dict_dicts = []
    for elem in list_dict_dicts:
        for key_translation, value_dicts in elem.items():
            bible_versions = [k for k, v in value_dicts.items() if v != '']
            for bible_pair in combinations(bible_versions, 2):
                source_bible = bible_pair[0]
                target_bible = bible_pair[1]
                new_list_dict_dicts.append({key_translation: {source_bible: value_dicts[source_bible],
                                                              target_bible: value_dicts[target_bible]}})

    return new_list_dict_dicts


def get_data_from_rj(
        task,
        corpus_dir="resource/defending-against-authorship-attribution-corpus/corpus",
        dev=False
):
    """
    Reads in texts and labels from the Riddell-Juola (RJ) corpus. The RJ version contains a control group, attacks of
    imitation and obfuscation, and three round-trip translation attacks (['translation_de', 'translation_ja',
    'translation_de_ja']). The translation attacks have testing samples are translated with Google Translate and
    share the same training examples with the control group.
    Args:
        task: a str, should be in ['control', 'imitation', 'obfuscation', 'backtranslation_de', 'backtranslation_ja',
            'backtranslation_de_ja', 'cross_validation']; when specified as 'cross_validation', all the training samples
             of ['control', 'imitation', 'obfuscation'] and no test samples will be returned
        corpus_dir: a str, path to RJ corpus
        dev: a bool, whether the first samples of each author is used as a dev sample, used in deep learning scenarios
    Returns:
        if not dev, four lists, text/label of train/test sets
        else, six lists, text/label of train/dev/test sets
    """

    def get_task_specific_data(task, dev):
        train_text_, train_label_, dev_text_, dev_label_ = [], [], [], []
        authors = [f.name.split(".")[0] for f in os.scandir(os.path.join(corpus_dir, "attacks_" + task)) if (not f.name.startswith(".") and f.name.split(".")[0] != 'cfeec8')]

        for dir_ in [os.path.join(corpus_dir, author) for author in authors]:
            for raw in os.scandir(dir_):
                if dev:
                    if not raw.name.endswith('_01.txt'):
                        train_text_.append(open(raw.path, 'r', encoding='utf8').read())
                        train_label_.append(raw.name.split('_')[0])
                    else:
                        dev_text_.append(open(raw.path, 'r', encoding='utf8').read())
                        dev_label_.append(raw.name.split('_')[0])
                else:
                    train_text_.append(open(raw.path, "r", encoding="utf8").read())
                    train_label_.append(raw.name.split("_")[0])
        # read in testing
        test_text_, test_label_ = zip(
            *[(open(f.path, "r", encoding=chardet.detect(open(f.path, "rb").read())["encoding"]).read(), f.name.split(".")[0])
                for f in os.scandir(os.path.join(corpus_dir, "attacks_" + task))
                if (".txt" in f.name and f.name.split(".")[0] != 'cfeec8')])  # cfeec8 does not have training data
        if not dev:
            return train_text_, train_label_, list(test_text_), list(test_label_)
        else:
            return train_text_, train_label_, dev_text_, dev_label_, list(test_text_), list(test_label_)

    train_text, train_label, dev_text, dev_label, test_text, test_label = [], [], [], [], [], []
    if task != "cross_validation":
        return get_task_specific_data(task, dev)
    else:
        for _task in ["imitation", "obfuscation", "control"]:
            if dev:
                train_text_, train_label_, dev_text_, dev_label_, _, _ = get_task_specific_data(_task, dev)
                train_text.extend(train_text_)
                train_label.extend(train_label_)
                dev_text.extend(dev_text_)
                dev_label.extend(dev_label_)
            else:
                train_text_, train_label_, _, _ = get_task_specific_data(_task, dev)
                train_text.extend(train_text_)
                train_label.extend(train_label_)
        if dev:
            return train_text, train_label, dev_text, dev_label, test_text, test_label
        else:
            return train_text, train_label, test_text, test_label


def get_data_from_ebg(task, corpus_dir="resource/Drexel-AMT-Corpus", dev=False):
    """
    Reads in texts and labels from the Extended Brennan-Greenstadt (EBG) corpus.
    Args:
        task: a str, should be in ['obfuscation', 'imitation', 'cross_validation']
        corpus_dir: a str, path to EBG corpus
        dev: a bool, whether the first samples of each author is used as a dev sample, used in deep learning scenarios
    Returns:
        if not dev, four lists, text/label of train/test sets
        else, six lists, text/label of train/dev/test sets
    """
    train_text, train_label, dev_text, dev_label, test_text, test_label = [], [], [], [], [], []

    for author in os.scandir(corpus_dir):
        if not author.name.startswith("."):
            if dev:
                train_text_, dev_text_ = [], []
                for f in os.scandir(author.path):
                    if re.match(r'[a-z]+_[0-9]{2}_*', f.name):
                        if f.name.endswith('_01_1.txt'):
                            dev_text_.append(open(f.path, 'r', encoding=chardet.detect(open(f.path, 'rb').read())[
                                'encoding']).read())
                        else:
                            train_text_.append(open(f.path, 'r', encoding=chardet.detect(open(f.path, 'rb').read())[
                                'encoding']).read())
                train_text.extend(train_text_)
                dev_text.extend(dev_text_)
                train_label.extend([author.name] * len(train_text_))
                dev_label.extend([author.name] * len(dev_text_))
            else:
                train_text.extend(
                    [open(f.path, "r", encoding=chardet.detect(open(f.path, "rb").read())["encoding"],).read()
                     for f in os.scandir(author.path) if re.match(r"[a-z]+_[0-9]{2}_*", f.name)])
                train_label.extend([author.name] * len([f.name for f in os.scandir(author.path)
                                                        if re.match(r"[a-z]+_[0-9]{2}_*", f.name)]))
            # read in testing
            if task == "imitation":
                test_text.extend(
                    [open(f.path, "r", encoding=chardet.detect(open(f.path, "rb").read())["encoding"]).read()
                     for f in os.scandir(author.path) if re.match(r"[a-z]+_imitation_01.txt", f.name)])
                test_label.append(author.name)
            elif task == "obfuscation":
                test_text.extend(
                    [open(f.path, "r", encoding=chardet.detect(open(f.path, "rb").read())["encoding"]).read()
                     for f in os.scandir(author.path) if re.match(r"[a-z]+_obfuscation.txt", f.name)])
                test_label.append(author.name)
            else:
                # when task_name == 'cross_validation'
                pass
    if dev:
        return train_text, train_label, dev_text, dev_label, test_text, test_label
    else:
        return train_text, train_label, test_text, test_label


def vectorize_writeprints_static(raws):
    """
    Extracts `writeprints-static` features from a list of texts. Done by PyPI library `writeprints-static` v0.0.2.
    Args:
        raws: a list of str.
    Returns:
         a np.array of writeprints-static numeric.
    """
    vec = WriteprintsStatic()
    features = vec.transform(raws)

    return features.toarray()


def vectorize_koppel512(raws):
    """
    Extracts `Koppel512` function words count.
    Args:
        raws: a list of str.
    Returns:
         a np.array of Koppel512 numeric.
    """
    function_words = open("resource/koppel_function_words.txt", "r").read().splitlines()

    return np.array(
        [
            [
                len(re.findall(r"\b" + function_word + r"\b", raw.lower()))
                for function_word in function_words
            ]
            for raw in raws
        ]
    )



def df2corpus(
              model_prefix: str = 'attacks_braiv_baseline',
              pkl_path: str = 'resource/adversarial_samples/rj_control/baseline_4bible.pkl',
              corpus_dir: str = "resource/defending-against-authorship-attribution-corpus/corpus"):
    """
    Saves adversarial samples in `resource/adversarial_samples` to the correponding corpus (i.e., rj and ebg).

    """
    DEFENSE_BIBLES = ['NIRV', 'NABRE', 'KJV', 'YLT']
    text_label = pickle.load(open(pkl_path, 'rb'))
    for bible in DEFENSE_BIBLES:
        os.mkdir(os.path.join(corpus_dir, f'{model_prefix}_{bible}'))
        for i, label in enumerate(text_label[bible]['labels']):
            with open(os.path.join(corpus_dir, f'{model_prefix}_{bible}', f'{label}.txt'), 'w') as f:
                f.write(text_label[bible]['texts'][i])
    print('Done.')

# df2corpus(
#           model_prefix = 'attacks_braiv_idio',
#           pkl_path= 'resource/adversarial_samples/rj_control/idio_4bible.pkl',
#           corpus_dir = "resource/defending-against-authorship-attribution-corpus/corpus")



#
# def pre_process(text):
#     """
#     Lowercase and lemmatize a sentence
#     :param text: sentence to preprocess
#     :return preprocessed sentence
#     """
#     # lemmatizer = WordNetLemmatizer()
#     # text = lemmatizer.lemmatize(text)
#     return re.sub(r'[^\w\s]', ' ', text).lower()
#
#
# def pinc(source, paraphrase, n=4):
#     """
#     Paraphrase In N-gram Changes (PINC) score measures the percentage of ngrams that appear in the candidate sentence
#     but not in the source sentence.
#     :param source: sentence a
#     :param paraphrase: sentence b
#     :param n: n-grams, default n=4 e.g a=[1,2,3] b=[2,3,4] => 2-grams of a: {(1,2),(2,3)}
#     :return PINC score between sentence (a) and sentence (b)
#     """
#     sum = 0
#     index = 0
#     for i in range(1, n + 1):
#         s = set(nltk.ngrams(source, i, pad_left=False, pad_right=False))
#         p = set(nltk.ngrams(paraphrase, i, pad_left=False, pad_right=False))
#         if s and p:
#             index += 1
#             intersection = s.intersection(p)
#             sum += 1 - len(intersection) / len(p)
#
#     if index == 0:
#         return 0
#
#     return sum / index
#
# def get_pinc_score(data):
#     """
#     calculates PINC scores over all collected utterances, introduced in Collecting Highly Parallel Data for Paraphrase Evaluation)
#     :param data: Dataset to be measured in terms of PINC. Python dictionary, key: initial utterance - value: list of paraphrases
#     :return Mean PINC score of a dictionary of paraphrases
#     """
#     index = 0
#     total_pinc = 0
#     pincs = []
#     expr_by_pinc = []
#     for expr in data:
#         tokens_1 = pre_process(expr).split(" ")
#         expr_pinc = 0
#         for p in data[expr]:
#             p = pre_process(p)
#             tokens_2 = p.split(" ")
#             expr_pinc += pinc(tokens_1, tokens_2)
#             index += 1
#
#         # check to avoid zero division
#         if len(data[expr]) != 0:
#             expr_pinc = expr_pinc / len(data[expr])
#         else:
#             continue
#         pincs.append(expr_pinc)
#         expr_by_pinc.append((expr, expr_pinc))
#         total_pinc += expr_pinc
#     expr_by_pinc.sort(key=lambda t: -t[1])
#     return {"Mean PINC": total_pinc / len(data),
#             "All PINCs": pincs,
#             "Expr by Pinc": expr_by_pinc}
